#!/usr/bin/env python3

"""

Script to access the Givenergy API over a time period
and download historical data to files for later processing.

The first argument can point to a configuration file containing
your inverter's code and your API key. See demo_system for
an example.

Alternatively, you can just add this into the system dict
below.


Copyright Robert Izzard 2024.

I take no responsibility for ANYTHING you do with this script
or any damage you cause with it. It was written in good faith
but you can check the code yourself.

Please read the LICENCE in this repository which covers this file
and all in the repostiory.

"""

import datetime
import inverter
import os
import requests
import sys
import time

############################################################
#
# You can either define your system properties
# here, or you can load them from a config file
# (see file: demo_system) the name of which is
# specified as the first argument to this
# script.
#
############################################################

system = {
    # your inverter code goes here :
    # you can get this from your Givenergy dashboard
    # by clicking on "inverter" and looking at the URL
    #
    # https://givenergy.cloud/inverter/<it is here>
    'inverter' : '',

    # your API key goes here
    #
    # You must make your own API key. From the dashboard
    # go to Account Settings > Manage Account Security >
    # Manage API Tokens > Generate New Token
    #
    # You must enable (at least) the following scopes:
    #
    # "api"
    # "api:inverter"
    # "api:inverter:data"
    #
    'APIkey' : ''
}

# setup with default options or use cmd-line passed
# options filename
system,options = inverter.get_options(system=system)

# API interaction settings
givAPI = inverter.default_API_settings(APIkey=system['APIkey'])

# make output directory
inverter.make_outdir(options['outdir'])

# loop over times until now
now = datetime.datetime.utcnow()
t = now - options['lookback_delta']
while t <= now:
    # set up time string
    timestring = t.strftime('%Y-%m-%d')
    print(timestring)

    # construct the URL to access the database
    url = inverter.givURL(givAPI,system,timestring)

    # call the server to get the JSON data
    r = requests.get(url,
                     headers = givAPI['headers'])
    if not r:
        sys.exit(f'Failed to get data from {url}')

    # convert to dict
    j = inverter.json_string_to_dict(r.text)

    # write indented JSON to the (possibly gzipped) file
    inverter.dumpdata(options,timestring,j)

    # loop over chunks of data
    for chunk in j['data']:
        # move to nicely-named strings
        _t = chunk['time']
        SOC = chunk['power']['battery']['percent']
        load = chunk['power']['consumption']['power']
        PV = chunk['power']['solar']['power']

        # output to the screen to show we're doing something
        print(f"{_t:>25s} {load:10} W {PV:10} W {SOC:10} %")

    # sleep to prevent server overload
    time.sleep(givAPI['sleeptime'])

    # update time and loop
    t += options['lookback_dt']
