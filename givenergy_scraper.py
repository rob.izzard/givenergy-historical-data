#!/usr/bin/env python3

"""
Script to scrape inverter data from givenergy's portal,
the idea being this is the complete dataset, not the
limited data given by their API.

First argument to the script should be the config
file (see "demo_system" for an example).

Please see the repository's LICENCE for licencing details.
"""

import configparser
import givenergy_scraper_functions
import os
import pprint
import sys
import time

from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# read in config filename as first argument
file = None
if len(sys.argv) > 1:
    file = sys.argv[1]

if file != None:
    config = configparser.ConfigParser(interpolation=None)
    config.read(file)
else:
    sys.exit("You must specify a configuration file as the first argument.")


inverter = config.get('INVERTER','inverter')
urls = {
    'login' : 'https://givenergy.cloud/login',
    'inverter data' : 'https://givenergy.cloud/inverter/'+inverter+'/data'
}

data = {
    'givenergy cloud' : {
        'username' : config.get('CLOUD','username'),
        'password' : config.get('CLOUD','password'),
        'inverter' : inverter,
    }
}

do_downloads = True # set to False for testing

def click(browser,element):
    # click an element
    browser.execute_script("arguments[0].click();", element)

# make browser options and output directory
opts = Options()
opts.add_argument('--headless')
opts.set_preference("browser.download.folderList",
                    2)
opts.set_preference("browser.download.manager.showWhenStarting",
                    False)
outdir = os.path.abspath(os.path.join(config.get('OPTIONS','outdir_scraped')))
givenergy_scraper_functions.make_outdir(outdir)
opts.set_preference("browser.download.dir",
                    outdir)
opts.set_preference("browser.helperApps.neverAsk.saveToDisk",
                    "text/csv")


# launch (virtual) browser and make its (virtual) window
# very large so the sidebar is "shown".
browser = Firefox(options=opts)
browser.set_window_size(1920,1080)

# Login to the Givenergy site
browser.get(urls['login'])
form = {
    'user' : givenergy_scraper_functions.finite_elements(browser,"//input[@data-qa='field.username']"),
    'password' : givenergy_scraper_functions.finite_elements(browser,"//input[@data-qa='field.password']"),
    'submit' : givenergy_scraper_functions.finite_elements(browser,"//span[@class='v-btn__content']"),
    'button' : givenergy_scraper_functions.finite_elements(browser,"//button"),
}

print(pprint.pprint(form),"\n\n\n")
givenergy_scraper_functions.screenshot(config,browser)

fillin = {
    'user' : data['givenergy cloud']['username'],
    'password' : data['givenergy cloud']['password']
}

for name,element in form.items():
    print(f"Element: {name}, type {type(element)}")

    if not isinstance(element,list):
        element_list = [element]
    else:
        element_list = element

    for element in element_list:
        print(f"tag_name : {element.tag_name}")
        print(f"text : {element.text}")
        print(f"parent : {element.parent}")
        print(f"location : {element.location}")
        print(f"size : {element.size}")
        print()

        if name in fillin:
            element.send_keys(fillin[name])


# fill in the form, see what we get
givenergy_scraper_functions.screenshot(config,browser)

# login
form['button'][0].click()

# wait for form to load
try:
    element = WebDriverWait(browser, 10).until(
        EC.title_contains((data['givenergy cloud']['inverter']))
        )
finally:
    givenergy_scraper_functions.screenshot(config,browser,message='login_timed_out')
givenergy_scraper_functions.screenshot(config,browser,message='end_login')

# get inverter data
print("Get inverter data")
browser.get(urls['inverter data'])
try:
    element = WebDriverWait(browser, 10).until(
        EC.title_contains((data['givenergy cloud']['inverter'] + ' - Data'))
    )
finally:
    givenergy_scraper_functions.screenshot(config,browser,message='inverter_data_timed_out')

print("Waiting for page to load...")
time.sleep(5)
givenergy_scraper_functions.screenshot(config,browser,message='inverter_data15')

# we're now at the inverter data page, great
#
# but this defaults to 15 lines : instead
# we want "All"

# get form item that should be "-1" (=="All")
print("get hidden item")
hidden = givenergy_scraper_functions.all_elements(
    browser,
    "//input[@aria-label='Rows per page:']//parent::div//parent::div//input[@type='hidden']")[0]

# Open the menu.
click(browser,hidden)
time.sleep(5)
givenergy_scraper_functions.screenshot(config,browser,message='inverter_data-submenu_opened')

# Find the "All" on the list and click it.
all = givenergy_scraper_functions.all_elements(browser,
                                              "//div[contains(text(),'All')]")[0]
click(browser,all)
time.sleep(5)
givenergy_scraper_functions.screenshot(config,browser,message='inverter_data-All_clicked')

# Loop over time until we run out of data.
prev_csv_date = None
while True:
    # Get data downloader URL.
    download_form = givenergy_scraper_functions.all_elements(browser,
                                                            "//form")[0]
    csv_data_url = download_form.get_attribute('action')
    print(f"CSV Data URL {csv_data_url}")

    csv_date = givenergy_scraper_functions.date_parse(csv_data_url)

    # Check if we should stop.
    if csv_date == None:
        sys.exit("No known date : stopping")

    if csv_date == prev_csv_date:
        sys.exit("Date has not changed : stopping")

    print(f"CSV date {csv_date}")

    # construct filename
    download_file = f"{inverter} Data Download {csv_date}.csv"
    download_file_path = os.path.join(outdir,download_file)
    print(f"Download to file {download_file}")

    # check we haven't already downloaded this: if so, stop
    checkfiles = [ download_file,
                   download_file + '.gz' ]
    for checkfile in checkfiles:
        path = os.path.join(outdir,checkfile)
        if os.path.exists(path):
            sys.exit(f"File {checkfile} at {path} exists: assume we've already download this and any older data, hence exit.")

    if do_downloads == True:

        # Hence download.
        download_button = givenergy_scraper_functions.all_elements(
            browser,
            "//form//parent::span//descendant::button")[0]
        download_button.click()

        print("Clicked download, waiting...")
        time.sleep(5)
        givenergy_scraper_functions.screenshot(config,browser,message='inverter_data-Download_clicked')

        # wait until the file is there
        while True:
            if os.path.exists(download_file_path):
                break
            else:
                print("Not there yet, waiting ...")
                time.sleep(1)

        print(f"File downloaded at {download_file_path}")

        # gzip the output
        if givenergy_scraper_functions.to_bool(config.get('OPTIONS','gzip')) == True:
            print("Gzip downloaded data")
            givenergy_scraper_functions.gzip_file(download_file_path)

    # Click to download previous data.
    # (There has to be a better xpath...!)
    previous_button = givenergy_scraper_functions.all_elements(
        browser,
        '//*[@id="main"]/div/div[4]/div[2]/div[1]/div[1]/div[1]/div/div[3]/div/div/div/button[1]')[0]
    print(f"Previous button {previous_button}")
    previous_button.click()
    time.sleep(5)

    # Save previous date.
    prev_csv_date = csv_date
