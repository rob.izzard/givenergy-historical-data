#!/usr/bin/env python3

"""

Script to process data read by get_inverter_data.py.

Copyright Robert Izzard 2024.

I take no responsibility for ANYTHING you do with this script
or any damage you cause with it. It was written in good faith
but you can check the code yourself.

Please read the LICENCE in this repository which covers this file
and all in the repostiory.

"""

import glob
import gzip
import inverter
import json
import os

# read in options
system,options = inverter.get_options()

# get time-sorted list of data files
datafiles = inverter.datafile_list(options['outdir'])

# loop over files
for datafile in datafiles:
    # open file
    with gzip.open(datafile,'r') as f:
        # read in as JSON
        j = json.load(f)

        # at each time, output
        for chunk in j['data']:
            _t = chunk['time']
            SOC = chunk['power']['battery']['percent']
            load = chunk['power']['consumption']['power']
            PV = chunk['power']['solar']['power']

            #
            # process your data here
            #

            # output to the screen
            print(f"{_t:>25s} {load:10} W {PV:10} W {SOC:10} %")
