# givenergy historical data

Script to access the Givenergy API over a time period
and download historical data to files for later processing.

The first argument can point to a configuration file containing
your inverter's code and your API key, as well as other options. 

See demo_system for an example.

Alternatively, you can just add this into the system dict
below.


I take no responsibility for ANYTHING you do with this script
or any damage you cause with it. It was written in good faith
but you can check the code yourself.


* get_API_data.py

This gets data from your inverter using the Givenergy API. 

* givenergy_scraper.py

This uses the Selenium library to log in to your Givenergy 
portal page and gets the inverter data from there.


Note that the scraper gets *more* data: the API does not give
you everything (at present).

However, please also note that the scraper most likely will
break when the HTML/Javascript of the portal changes. 
You have been warned!
