"""

Support functions for Givenergy parser

"""
import gzip
import os
import pathlib
import re
import shutil

from dateutil import parser
from selenium.webdriver.common.by import By

def date_parse(string):
    """
    Given a string, try to extract a date
    """
    x = None
    try:
        x = parser.parse(string,fuzzy=True)
    except:
        try:
            x = parser.parse(string,fuzzy_with_tokens=True)
        except:
            m = re.search(r'\d{4}-\d{2}-\d{2}', string)
            if m:
                return m.group()
            else:
                return None
    return x

def finite_size(lst):
    # return elements of the list with non-zero size
    return list(filter(lambda x : x.size['height']>0 and x.size['width']>0,
                       lst))

def all_elements(browser,xpath):
    # return list of all elements matching xpath
    return list(browser.find_elements(By.XPATH,xpath))

def finite_elements(browser,xpath):
    # return elements matching xpath with finite size
    return finite_size(browser.find_elements(By.XPATH,xpath))


def make_outdir(dir):
    """
Make output directory, if it doesn't already exist.
    """
    if not os.path.exists(dir):
        try:
            os.mkdir(dir)
        except:
            sys.exit(f"failed to make data output dir at {dir}")

def gzip_file(path,unlink=True):
    """
    Gzip file at path
    """
    gzpath = pathlib.Path(path).with_suffix('.csv.gz')
    with open(path, 'rb') as f_in:
        with gzip.open(gzpath, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    if unlink:
        os.unlink(path)

def to_bool(x):
    return x in ("True", "true", True)

screenshot_counter = 0
def screenshot(config,browser,message=None):
    """
    Save a screenshot from the "browser".
    """
    global screenshot_counter
    if config.get('OPTIONS','screenshots') == True:
        screenshot_counter += 1
        if message:
            file = f"giv{screenshot_counter}_{message}.png"
        else:
            file = f"giv{screenshot_counter}.png"
        browser.save_screenshot(file)
