"""

Helper functions for Rob's Givenergy inverter scripts.


Copyright Robert Izzard 2024.

I take no responsibility for ANYTHING you do with this script
or any damage you cause with it. It was written in good faith
but you can check the code yourself.

Please read the LICENCE in this repository which covers this file
and all in the repostiory.

"""

import configparser
import datetime
import glob
import gzip
import json
import os
import re
import sys

def get_options(file=None,
                system=None):
    """
Default set of options, or load from file.
    """
    options = {
        # how many days to look back?
        'lookback_delta' : 365,
        # get data every day (this is forced by the API)
        'lookback_dt' : 1,
        # output directory, relative to the directory we start in
        'outdir_API' : 'data_API',
        # gzip output
        'gzip' : True,
    }

    if system == None:
        # default system
        system = {
            'inverter' : '',
            'APIkey' : ''
        }

    def to_bool(x):
        return x in ("True", "true", True)

    # read in from first command-line argument if no
    # file given
    if file == None and len(sys.argv) > 1:
        file = sys.argv[1]

    # override options, if specified
    if file:
        config = configparser.ConfigParser(interpolation=None)
        config.read(file)
        for k in system:
            try:
                system[k] = config.get('INVERTER',k)
            except:
                pass
        for k in options:
            try:
                options[k] = config.get('OPTIONS',k)
            except:
                pass

    # convert options to appropriate data types
    options['lookback_delta'] = datetime.timedelta(days=int(options['lookback_delta']))
    options['lookback_dt'] = datetime.timedelta(days=int(options['lookback_dt']))
    options['gzip'] = to_bool(options['gzip'])

    # return
    return system,options

def datafile_list(dir):
    """
Return time-sorted list of datafiles
    """
    datafiles = glob.iglob(os.path.join(dir,'*'))
    def _sort_dates(dates):
        def _date_key(date_string):
            match = re.search(r'\d{4}-\d{2}-\d{2}', date_string)
            return datetime.datetime.strptime(match.group(),
                                              '%Y-%m-%d')
        return sorted(dates, key=_date_key)
    return _sort_dates(datafiles)

def make_outdir(dir):
    """
Make output directory, if it doesn't already exist.
    """
    if not os.path.exists(dir):
        try:
            os.mkdir(dir)
        except:
            sys.exit(f"failed to make data output dir at {dir}")


def default_API_settings(APIkey=None,
                         sleeptime=1,
                         url="https://api.givenergy.cloud/v1/inverter/",
                         pageSize=100000):
    """
Default settings for communicating with the givenergy API.
    """
    if APIkey == None:
        sys.exit("You must specify an API key when setting up the API settings")

    return {
        'url' : url,
        'parameters' : {
            'pageSize' : pageSize
        },
        'headers' :  {
            'Authorization' : 'Bearer ' + APIkey,
            'Content-Type' : 'application/json',
            'Accept' : 'application/json'
        },
        # time between calls to the API, in integer seconds, to
        # prevent hitting the max of 300 per minute. One second
        # should be plenty!
        'sleeptime' : sleeptime
    }


def json_string_to_dict(text):
    """
Convert JSON text string to Python dict
    """
    j = None
    try:
        j = json.loads(text)
    except:
        # failed, try conversions
        for s in ('False','True','None'):
            text = text.replace(s,'"'+s+'"')
        try:
            j = json.loads(text)
        except:
            sys.exit("Failed to load JSON data : please check it is valid JSON")
    return j


def givURL(givAPI,system,timestring):
    """
Function to make the URL to interact with the givenergy
database.
    """
    # set up the array used to make the URL
    _x = [
        givAPI['url'],
        system['inverter'],
        '/data-points/',
        timestring,
        '?'
        ]
    for p in givAPI['parameters']:
        _x.extend([p,'=',givAPI['parameters'][p]])

    # hence the URL for the request
    return ''.join(str(_) for _ in _x)


def dumpdata(options,timestring,j):
    """
Dump dict in j to file labelled by the timestring
    """
    if options['gzip']:
        file = os.path.join(options['outdir_API'],
                            timestring + '.gz')
        f = gzip.open(file,'wt')
    else:
        file = os.path.join(options['outdir_API'],
                            timestring)
        f = open(file,'w')
    json.dump(j,f,indent=2)
    f.close()
